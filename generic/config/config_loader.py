import os


class ConfigLoader:

    ENV = os.getenv("env")

    @classmethod
    def load_config(cls):
        from generic.config.environment_config import DevConfig
        from generic.config.environment_config import StageConfig
        from generic.config.environment_config import ProdConfig

        config_by_env = {
            "dev": DevConfig,
            "prod": ProdConfig,
            "stage": StageConfig
        }

        return config_by_env[cls.ENV]()

