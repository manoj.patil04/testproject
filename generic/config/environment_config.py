# //TODO move sensitive data to env variables and secrets in CI.
class DevConfig:
    environment = "dev"
    url = 'https://optimize-ui-dev.claritas-nonprod.com/'


class StageConfig:
    environment = "stage"
    url = 'https://optimize-ui-stg.claritas-nonprod.com/'


class ProdConfig:
    environment = "prod"
    url = ''
