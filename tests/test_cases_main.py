import logging
from pages.login_page import LoginPage
from pages.base_page import Optimize_UI
from pages.create_property import PropertyPage
from pages.create_advertiser import AdvertiserPage
from pages.run_report import RunReportPage
from generic.config.config_loader import ConfigLoader
import pytest


class Tests:

    @classmethod
    def setup_class(cls):
        cls.EnvConfig = ConfigLoader.load_config()

    @pytest.fixture
    def load_pages(self,driver):
        base_page = Optimize_UI(driver)
        self.page.go_to_main_page()
        
    # Function to test login is successful
    def test_case_login(self, driver):
        base_page = Optimize_UI(driver)
        login_page = LoginPage(driver)
        base_page.load_url()
        login_page.login("manoj.patil@claritas.com","3l?njZxr")
        assert 'MY DASHBOARD' in login_page.verify_dashboard_text()




