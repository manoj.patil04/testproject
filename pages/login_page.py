from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import WebDriverWait
from pages import base_page
import time

class LoginPage:
    USERNAME_INPUT = (By.CSS_SELECTOR, "input[name ='username']")
    PASSWORD_INPUT = (By.CSS_SELECTOR, "input[name ='password']")
    DASHBOARD_PAGE_TEXT = (By.CLASS_NAME,   "breadCrumbBody")
    LOGIN_PAGE_TEXT = (By.CLASS_NAME,   "amplify-alert__body")

    def __init__(self, browser):
        self.browser = browser
        self.wait = WebDriverWait(browser, 10)
        self.BASE_PAGE = base_page.Optimize_UI(self.browser)

    def login(self,uname,passwrd):
        username = self.browser.find_element(*self.USERNAME_INPUT)
        password = self.browser.find_element(*self.PASSWORD_INPUT)
        username.click()
        username.clear()
        username.send_keys(uname)
        password.click()
        password.clear()
        password.send_keys(passwrd,Keys.ENTER)
        self.BASE_PAGE.wait_for_element(self.DASHBOARD_PAGE_TEXT)
        self.browser.save_screenshot('./reports/screenshots/Dashboard.png')
    
    def login_failed(self,uname,passwrd):
        username = self.browser.find_element(*self.USERNAME_INPUT)
        password = self.browser.find_element(*self.PASSWORD_INPUT)
        username.click()
        username.clear()
        username.send_keys(uname)
        password.click()
        password.clear()
        password.send_keys(passwrd,Keys.ENTER)
        self.BASE_PAGE.wait_for_element(self.Login_PAGE_TEXT)
        self.browser.save_screenshot('./reports/screenshots/Login_Failed.png')
    
    def sign_out(self,uname,passwrd):
        username = self.browser.find_element(*self.USERNAME_INPUT)
        password = self.browser.find_element(*self.PASSWORD_INPUT)
        username.click()
        username.clear()
        username.send_keys(uname)
        password.click()
        password.clear()
        password.send_keys(passwrd,Keys.ENTER)
        self.BASE_PAGE.wait_for_element(self.DASHBOARD_PAGE_TEXT)
        self.browser.execute_script("document.getElementsByClassName('dropdown dropdownMenu')[0].classList.add('show');")
        self.browser.execute_script("document.getElementsByClassName('dropdownMenu dropdownRight')[0].classList.add('show');")
        time.sleep(2)
        self.browser.execute_script("document.getElementsByClassName('cursorPointer')[2].click();")
        time.sleep(1)
        self.browser.save_screenshot('./reports/screenshots/Logout.png')

    def verify_dashboard_text(self):
        sign_out_text = self.browser.find_element(*self.DASHBOARD_PAGE_TEXT).text
        return sign_out_text

    def verify_login_failed_text(self):
        sign_out_text = self.browser.find_element(*self.LOGIN_PAGE_TEXT).text
        return sign_out_text
