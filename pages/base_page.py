import os
from random import randint
from generic.config.config_loader import ConfigLoader
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class Optimize_UI:
    EnvConfig = ConfigLoader.load_config()
    URL = EnvConfig.url

    def __init__(self, browser):
        self.browser = browser
        self.wait = WebDriverWait(browser, 15)

    def load_url(self):
        self.browser.get(self.URL) 
        
        
    def wait_for_element(self, element):
        self.wait.until(EC.presence_of_element_located(element))

    def get_login_text(self):
        login_text = self.browser.find_element(*self.LOGIN_PAGE_TEXT).text
        return login_text
    
    
    
    
